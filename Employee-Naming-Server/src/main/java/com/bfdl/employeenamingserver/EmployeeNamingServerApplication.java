package com.bfdl.employeenamingserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EmployeeNamingServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeNamingServerApplication.class, args);
	}

}
