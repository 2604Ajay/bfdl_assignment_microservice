package com.bfdl.employeemanagemnt.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.bfdl.employeemanagemnt.employee.Address;
import com.bfdl.employeemanagemnt.employee.Employee;
import com.bfdl.employeemanagemnt.employee.EmployeeDto;
import com.bfdl.employeemanagemnt.service.EmployeeServiceImpl;


@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeServiceImpl employeeService;

	
	@GetMapping("/")
	public String getHome() {
		return "Home for Employee Managment";
	}

	@PostMapping("/add")
	public ResponseEntity<Employee> addEmployee(@RequestBody EmployeeDto emp) {
		
		System.out.println(emp.toString());
		
		RestTemplate restTemplate=new RestTemplate();
		
		String url="http://localhost:8200/address/get/"+emp.getPincode();
		ResponseEntity<Address> addressResponse= restTemplate.getForEntity(url, Address.class);
//		Address address=new Address(415709L, "Khed", "Maharashtra");
		
		return new ResponseEntity<Employee>(employeeService.addEmployee(new Employee(emp.getFullName(), addressResponse.getBody())), HttpStatus.CREATED);
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<Employee> getEmployee(@PathVariable ("id") Integer empid) {

		return new ResponseEntity<Employee>(employeeService.getemployee(empid), HttpStatus.FOUND);
	}

	@GetMapping("/get")
	public ResponseEntity<List<Employee>> getEmployees() {

		return new ResponseEntity<List<Employee>>(employeeService.getEmployees(), HttpStatus.FOUND);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<List<Employee>> deleteEmployees(@PathVariable("id") Integer empId) {

		employeeService.deleteemployee(empId);
		return getEmployees();
	}
	@PostMapping("/update")
	public ResponseEntity<Employee> updateEmployee(@RequestBody EmployeeDto emp) {

		Address address=new Address(415709L, "Khed", "Maharashtra");
		return new ResponseEntity<Employee>(employeeService.updateEmployee(new Employee(emp.getFullName(), address)), HttpStatus.CREATED);
	}


}
