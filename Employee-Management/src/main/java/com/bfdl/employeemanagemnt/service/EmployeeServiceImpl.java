package com.bfdl.employeemanagemnt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bfdl.employeemanagemnt.employee.Employee;
import com.bfdl.employeemanagemnt.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeManagementInterface{

	@Autowired
	private EmployeeRepository employeeRepo;

	@Override
	public Employee addEmployee(Employee emp) {
		
		return employeeRepo.save(emp);
	}

	@Override
	public List<Employee> getEmployees() {
		
		return employeeRepo.findAll();
	}

	@Override
	public Employee getemployee(Integer empid) {
		
		return employeeRepo.findById(empid).get();
	}

	@Override
	public void deleteemployee(Integer empId) {
		employeeRepo.deleteById(empId);
		
	}

	@Override
	public Employee updateEmployee(Employee emp) {
		
		return employeeRepo.save(emp);
	}
	
	
}
