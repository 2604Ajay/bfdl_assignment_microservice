package com.bfdl.employeemanagemnt.service;

import java.util.List;

import com.bfdl.employeemanagemnt.employee.Employee;

public interface EmployeeManagementInterface {

	public Employee addEmployee(Employee emp);
	public List<Employee> getEmployees();
	public Employee getemployee(Integer empid);
	public void deleteemployee(Integer empId);
	public Employee updateEmployee(Employee emp);
	
}
