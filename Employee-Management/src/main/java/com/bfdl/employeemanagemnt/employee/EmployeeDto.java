package com.bfdl.employeemanagemnt.employee;

import org.springframework.stereotype.Component;

@Component
public class EmployeeDto {

	private String fullName;
	private Long pincode;
	
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Long getPincode() {
		return pincode;
	}
	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}
	
	
	public EmployeeDto(String fullName) {
		super();
		this.fullName = fullName;
	}
	public EmployeeDto() {
		
	}
	@Override
	public String toString() {
		return "EmployeeDto [fullName=" + fullName + ", pincode=" + pincode + "]";
	}
	
}
