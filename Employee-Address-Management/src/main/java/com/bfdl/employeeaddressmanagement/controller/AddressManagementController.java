package com.bfdl.employeeaddressmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bfdl.employeeaddressmanagement.model.Address;
import com.bfdl.employeeaddressmanagement.service.AddressServiceinterface;

@RestController
@RequestMapping("/address")
public class AddressManagementController {

	@Autowired
	private AddressServiceinterface service;
	
	@GetMapping("/")
	public String getHome() {
		return "Address Management Home";
	}
	
	@PostMapping("/add")
	public ResponseEntity<Address> addAddress(@RequestBody Address address){
		return new ResponseEntity<Address>(service.addAddress(address),HttpStatus.CREATED);
	}
	
	@GetMapping("/get")
	public ResponseEntity<List<Address>> getAddress(){
	
		return new ResponseEntity<List<Address>>(service.getall(),HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/get/{pincode}")
	public ResponseEntity<Address> getAddress(@PathVariable ("pincode")Long pincode ){
		return new ResponseEntity<Address>(service.getAddress(pincode),HttpStatus.FOUND);
	}
	
	@DeleteMapping("/delete/{pincode}")
	public void deleteAddress(@PathVariable ("pincode")Long pincode ){
		service.deleteAddress(pincode);
	}
	
	@PutMapping("/update")
	public ResponseEntity<Address> updateAddress(@RequestBody Address address){
		return new ResponseEntity<Address>(service.updateAddress(address),HttpStatus.FOUND);
	}
}
