package com.bfdl.employeeaddressmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;



@SpringBootApplication
@EnableDiscoveryClient
public class EmployeeAddressManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeAddressManagementApplication.class, args);
	}

}
