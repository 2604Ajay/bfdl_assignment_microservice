package com.bfdl.employeeaddressmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bfdl.employeeaddressmanagement.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>{

}
