package com.bfdl.employeeaddressmanagement.service;

import java.util.List;

import com.bfdl.employeeaddressmanagement.model.Address;

public interface AddressServiceinterface {

	public Address addAddress(Address address);
	public Address getAddress(Long pincode);
	public void deleteAddress(Long pincode);
	public Address updateAddress(Address address);
	public List<Address>getall();
	
}
