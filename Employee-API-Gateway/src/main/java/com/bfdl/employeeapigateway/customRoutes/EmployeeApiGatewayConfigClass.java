package com.bfdl.employeeapigateway.customRoutes;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmployeeApiGatewayConfigClass {

	@Bean
	public RouteLocator getRoutes(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(
				p->p.path("/employee/**")
				.uri("lb://EMPLOYEE-MANAGEMENT"))
				.route(
						p->p.path("/address/**")
						.uri("lb://EMPLOYEE-ADDRESS-MANAGEMENT"))
				.build();
	}
}
